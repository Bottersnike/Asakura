import os
import shutil

from cogs.util.bot import Asakura


def main():
    bot = Asakura()
    bot.run()


if __name__ == '__main__':
    main()
