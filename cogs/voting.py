import itertools
import random
import math
import os

import discord

from collections import defaultdict

from ruamel import yaml
from discord.ext import commands


RESP_DIR = 'config/responses'
PERCENTAGE = 0.85


class Voting:
    def __init__(self, bot):
        self.bot = bot
        with open('config/votes.yml', 'r') as votes:
            self.votes = yaml.load(votes, Loader=yaml.Loader)

    def get_count(self, filename):
        acc = 0
        for vote in self.votes['votes'].values():
            for pair in vote:
                if filename in pair:
                    acc += 1

        for pair in self.votes['slides'].values():
            if pair != -1 and filename in pair:
                acc += 0.5

        return acc

    @commands.command()
    async def vote(self, ctx, response=None):
        """Use `vote` to get a fresh slide
        Vote on a slide with `vote a` or `vote b`
        """
        if response is not None: response = response.lower()
        if response not in ['a', 'b', None]:
            return await ctx.send('I was expecting the vote to be `a` or `b`')

        if ctx.author.id in self.votes['slides'] and self.votes['slides'][ctx.author.id] == -1:
            return await ctx.send('That\'s them all! Thanks for voting.')

        if not isinstance(ctx.channel, discord.abc.PrivateChannel):
            try: await ctx.message.delete()
            except discord.Forbidden: pass
            return await ctx.send('Voting only works in DMs. Sorry about that.')

        create_new = False
        if ctx.author.id in self.votes['slides'] and response:
            if ctx.author.id not in self.votes['votes']:
                self.votes['votes'][ctx.author.id] = []
            votes = self.votes['votes'][ctx.author.id]

            v = self.votes['slides'][ctx.author.id]
            votes.append((v[0], v[1]) if response == 'a' else (v[1], v[0]))
            create_new = True
        elif ctx.author.id not in self.votes['slides']:
            create_new = True

        if create_new:
            responses = []
            for path in os.listdir(RESP_DIR):
                file = os.path.join(RESP_DIR, path)
                if os.path.isfile(file) and file[0] != '.':
                    responses.append((self.get_count(file), file))

            responses.sort()
            fewest = [x for x in responses if x[0] == responses[0][0]]
            rest = [x for x in responses if x not in fewest]
            random.shuffle(fewest)

            def voted_before(a, b):
                try:
                    for vote in self.votes['votes'][ctx.author.id]:
                        if a[1] in vote and b[1] in vote:
                            return True
                except KeyError:
                    self.votes['votes'][ctx.author.id] = []
                return False

            slide = None
            for x, y in itertools.product(fewest, fewest):
                if x == y: continue
                if x != y and not voted_before(x, y):
                    slide = (x[1], y[1])
                    break

            if slide is None:
                for x, y in itertools.product(fewest + rest, fewest + rest):
                    if x == y: continue
                    if x != y and not voted_before(x, y):
                        slide = (x[1], y[1])
                        break

            if slide is None:
                self.votes['slides'][ctx.author.id] = -1
                return await ctx.send('That\'s them all! Thanks for voting.')

            self.votes['slides'][ctx.author.id] = slide

        def count_lines(lines):
            return len(list(filter(lambda x: bool(x.strip()), lines.encode().rstrip().split(b'\n'))))

        slide = self.votes['slides'][ctx.author.id]
        async def send_half(slide_part, code):
            try:
                content = open(slide_part, 'r').read()
            except UnicodeDecodeError:
                content = open(slide_part, 'rb').read()
            lang = slide_part.split(".")[-1]

            d = f'Pick the better one:\n:regional_indicator_{code}: '
            d += '**Not working** ' if slide_part in self.votes['broke'] else ''
            if slide_part in self.votes['scores']:
                d += f'_Score: {self.votes["scores"][slide_part]}_ '
            d += f'{count_lines(content)} line(s)'

            if len(content) < 1900:
                d += f'```{lang}\n{content}```\n'
                return await ctx.send(d)

            d += f'```{lang}\n{content[:1850]}```\n'
            await ctx.send(
                content=f'{d} *Too much content, see attached file*',
                file=discord.File(open(slide[0], 'rb'),
                                  filename=f'{code}.{lang}')
            )

        await send_half(slide[0], 'a')
        await send_half(slide[1], 'b')

    @vote.after_invoke
    async def save(self, *args):
        with open('config/votes.yml', 'w') as votes:
            yaml.dump(self.votes, votes)

    @commands.command()
    @commands.is_owner()
    async def fully_voted(self, ctx):
        msg = ', '.join(self.bot.get_user(k).name
                        for k, v in self.votes['slides'].items() if v == -1)
        return await ctx.send(msg if msg else 'None')

    @commands.command()
    @commands.is_owner()
    async def results(self, ctx):
        def get_percentage(response):
            return 100 * sum(response['votes']) / response['count']
        def ordinal(n):
            return 'ᵗˢⁿʳʰᵗᵈᵈ'[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4]

        score = defaultdict(lambda: [0, 0])

        responses = [os.path.join(RESP_DIR, path)
                     for path in os.listdir(RESP_DIR)]

        votes = {}
        for i in self.votes['votes']:
            sc = defaultdict(lambda: [0, 0])
            for a, b in self.votes['votes'][i]:
                sc[a][0] += 1
                sc[a][1] += 1
                sc[b][1] += 1

            res = []
            for j in sc:
                res.append((j.split('/')[-1], sc[j][0] / sc[j][1]))
            res.sort(key=lambda x: x[1], reverse=True)

            weight = (len(self.votes['votes'][i])) / (len(responses) ** 2 - len(responses)) * 2
            for i in res:
                score[i[0]][0] += i[1] * weight
                score[i[0]][1] += weight

        res = []
        for i in score:
            res.append((i, score[i][0] / score[i][1]))
        res.sort(key=lambda x:x[1], reverse=True)

        for n, response in enumerate(res):
            symbol = ordinal(n + 1)

            dead = n > len(responses) * PERCENTAGE
            try:
                content = open(os.path.join(RESP_DIR, response[0]), 'r').read()
            except UnicodeDecodeError:
                content = open(os.path.join(RESP_DIR, response[0]), 'rb').read()

            lang = response[0].split(".")[-1]
            file = None
            if len(content) > 1800:
                content = '*Too much content, see attached file*'
                content += f'```{lang}\n{content[:1800]}```\n'
                file = discord.File(open(os.path.join(RESP_DIR, response[0]), 'r'), filename=f'response.{lang}')
            else:
                content = f'```{lang}\n{content}```\n'

            msg = '{}\n{} **{}{} place**: {}\n**<@{}>** ({}%)'.format(
                '=' * 50,
                ':skull_crossbones:' if (dead and n != 0) else ':white_check_mark:',
                n + 1, symbol, content,
                response[0].split('.')[0].split('/')[-1],
                round(response[1] * 100, 2)
            )
            await ctx.send(msg, file=file)
            with open('results.txt', 'a+') as results_file:
                results_file.write(msg+'\n\n\n')


def setup(bot):
    bot.add_cog(Voting(bot))
