import random
import time

from discord.ext import commands


API_URL = 'https://a.4cdn.org/{board}/{page}.json'
IMAGE_URL = 'https://i.4cdn.org/c/{tim}{ext}'
THUMB_URL = 'https://t.4cdn.org/c/{tim}s.jpg'
REFRESH_EVERY = 5


class FourChan:
    def __init__(self, bot):
        self.bot = bot
        self.boards = {}
        self.last_check = 0

    async def get_board(self, board):
        if board not in self.boards:
            self.boards[board] = ({}, 0)
        if time.time() - self.boards[board][1] > REFRESH_EVERY:
            async with self.bot.session.get(API_URL.format(board=board, page=1)) as resp:
                self.boards[board] = (await resp.json(), time.time())

        return self.boards[board][0]

    @commands.command()
    async def c(self, ctx):
        data = await self.get_board('c')

        posts = [
            thread['posts'][0] for thread in data.get('threads', {})
                               if thread['posts'][0].get('tim') is not None
        ]
        if not posts:
            return await ctx.send('Um? Somehow I found no results. Try again later.')

        post = random.choice(posts)
        tmb_url = THUMB_URL.format(tim=post.get('tim'))
        full_url = IMAGE_URL.format(tim=post.get('tim'), ext=post.get('ext', ''))
        return await ctx.send(f'{tmb_url} _Full resolution: <{full_url}>._')


def setup(bot):
    bot.add_cog(FourChan(bot))
